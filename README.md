# fizzbuzzpy

Playing with FizzBuzz in Python.

Structure based on [this article](https://dev.to/codemouse92/dead-simple-python-project-structure-and-imports-38c6).

## Setup

After pulling the code locally, make sure you current working directory is the appliation.

* Install [pipenv](https://pypi.org/project/pipenv/)
* Create your virtual environment: `pipenv --python 3.7`
* Install dependencies: `pipenv install --dev`

Once you are set up, you'll need to run `pipenv shell` to activate the environment.

## Makefile

Use the `make` command to execute the make rules set up in the Makefile.
