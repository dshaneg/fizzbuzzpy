import sys

from fizzbuzz.evaluator import Evaluator

DEFAULT_MAX = 100


def get_max():
    if len(sys.argv) == 2:
        max_number = int(sys.argv[1])
    else:
        max_number = DEFAULT_MAX

    return max_number


def main():
    # List of tuples that each contain a number to test for divisibility
    # and the text to print if the test number is divisible by the first
    # property.
    # The tuples are evaluated in order for each number tested.
    divisor_maps = [
        (3, 'Fizz'),
        (5, 'Buzz'),
    ]

    evaluator = Evaluator(divisor_maps)

    for i in range(1, get_max() + 1):
        print(evaluator.evaluate(i))


if __name__ == '__main__':
    main()
