class Evaluator():
    def __init__(self, divisor_maps):
        self.__divisor_maps = divisor_maps or []

    def evaluate(self, number):
        output = ''

        for divisor_map in self.__divisor_maps:
            if Evaluator.__divisible(number, divisor_map[0]):
                output = output + divisor_map[1]

        if (output == ''):
            output = str(number)

        return output

    @staticmethod
    def __divisible(dividend, divisor):
        return dividend % divisor == 0
