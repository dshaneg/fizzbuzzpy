import unittest

from fizzbuzz.evaluator import Evaluator


class TestEvaluatorFizzBuzz(unittest.TestCase):
    def setUp(self):
        divisor_maps = [
            (3, 'Fizz'),
            (5, 'Buzz'),
        ]
        self.evaluator = Evaluator(divisor_maps)

    def test_fizz(self):
        self.assertEqual('Fizz', self.evaluator.evaluate(9))

    def test_buzz(self):
        self.assertEqual('Buzz', self.evaluator.evaluate(10))

    def test_fizzbuzz(self):
        self.assertEqual('FizzBuzz', self.evaluator.evaluate(15))

    def test_neither(self):
        self.assertEqual('14', self.evaluator.evaluate(14))


class TestEvaluatorEdges(unittest.TestCase):
    def test_single_map(self):
        divisor_maps = [
            (3, 'Fizz')
        ]
        self.evaluator = Evaluator(divisor_maps)
        self.assertEqual('Fizz', self.evaluator.evaluate(9))

    def test_empty_map_list(self):
        divisor_maps = []
        self.evaluator = Evaluator(divisor_maps)
        self.assertEqual('9', self.evaluator.evaluate(9))

    def test_none_map_list(self):
        divisor_maps = None
        self.evaluator = Evaluator(divisor_maps)
        self.assertEqual('9', self.evaluator.evaluate(9))


if __name__ == '__main__':
    unittest.main()
