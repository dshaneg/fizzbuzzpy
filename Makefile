lint:
	pipenv run flake8

test:
	python -m unittest -v

run:
	python -m fizzbuzz 15

run_default:
	python -m fizzbuzz

tree:
	tree  -aI '.git|__pycache__'